import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AmountTextField extends StatefulWidget{

  final String title;
  final String text;
  final TextStyle textStyle;
  final Size size;

  AmountTextField(this.title, this.text, this.textStyle, this.size);

  @override
  AmountTextFieldState createState() => AmountTextFieldState();
}

class AmountTextFieldState extends State<AmountTextField> {

  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 5,
      margin: EdgeInsets.all(10),
      child: Container(
        width: widget.size.width / 2.3,
        height: widget.size.height / 10,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.title,
              style: TextStyle(color: Colors.grey),
            ),
            Text(
                widget.text != null
                    ? widget.text
                    : '0',
                style: widget.textStyle)
          ],
        ),
      ),
    );
  }
}