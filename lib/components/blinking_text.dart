import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BlinkingText extends StatefulWidget {
  final String _target;
  final TextStyle textStyle;
  BlinkingText(this._target, this.textStyle);
  @override
  BlinkingTextState createState() => BlinkingTextState();
}

class BlinkingTextState extends State<BlinkingText> {
  bool show = true;
  Timer timer;

  @override
  void initState() {
    timer = Timer.periodic(Duration(milliseconds: 700), (_) {
      setState(() => show = !show);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Text(widget._target,
      style: show
          ? widget.textStyle
          : TextStyle(color: Colors.transparent, fontSize: 14));

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }
}