
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lotosuiteplay/services/phone_formatter.dart';

class PhoneInputField extends StatelessWidget {
  IconData icon;
  String hintText;
  String prefixText;
  String suffixText;
  TextInputType textInputType;
  Color textFieldColor, iconColor;
  bool obscureText;
  double bottomMargin;
  TextStyle textStyle, hintStyle;
  var validateFunction;
  var onSaved;
  Key key;
  final UsNumberTextInputFormatter phoneNumberFormatter = UsNumberTextInputFormatter();

  //passing props in the Constructor.
  PhoneInputField(
      {this.key,
        this.hintText,
        this.obscureText,
        this.textInputType,
        this.textFieldColor,
        this.icon,
        this.iconColor,
        this.bottomMargin,
        this.textStyle,
        this.validateFunction,
        this.onSaved,
        this.hintStyle,
        this.prefixText});

  @override
  Widget build(BuildContext context) {
    return (new Container(
        margin: new EdgeInsets.only(bottom: bottomMargin),
        child: new DecoratedBox(
          decoration: new BoxDecoration(
              borderRadius: new BorderRadius.all(new Radius.circular(10.0)),
              color: textFieldColor),
          child: new TextFormField(
            style: textStyle,
            key: key,
            obscureText: obscureText,
            keyboardType: textInputType,
            validator: validateFunction,
            onSaved: onSaved,
            maxLines: 1,
            /*inputFormatters: <TextInputFormatter> [
              WhitelistingTextInputFormatter.digitsOnly,
              // Fit the validating format.
              phoneNumberFormatter,
            ],*/
            decoration: new InputDecoration(
              border: InputBorder.none,
              hintText: hintText,
              hintStyle: hintStyle,
              prefixText: prefixText,
              icon: new Icon(
                icon,
                color: iconColor,
              ),
            ),
          ),
        )));
  }
}