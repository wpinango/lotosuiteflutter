class Assets {
  static const lotoplay = 'assets/lotoplay3.png';
  static const backgroundLogin = 'assets/fondo1.png';

  static String getAssetName(String name) {
    return 'assets/chips/' + name + '.png';
  }
}