import 'package:diacritic/diacritic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lotosuiteplay/models/chip.dart';
import 'package:lotosuiteplay/theme.dart';
import 'package:lotosuiteplay/utils/conversions.dart';

class ChipsDialog extends StatefulWidget {
  final int raffle;
  final int columnCount;
  final List<Widget> headerImages;
  final List<LotoChip> chips;
  final ValueChanged<List<PreviewPlay>> onSelectedChipsChanged;

  const ChipsDialog(
      {Key key,
      this.chips,
      this.headerImages,
      this.columnCount,
      this.raffle,
      this.onSelectedChipsChanged})
      : super(key: key);

  @override
  ChipsDialogState createState() => new ChipsDialogState(
      raffle, columnCount, headerImages, chips, onSelectedChipsChanged);
}

class ChipsDialogState extends State<ChipsDialog> {
  int raffle;
  int columnCount;
  List<Widget> headerImages;
  List<LotoChip> chips;
  ValueChanged<List<PreviewPlay>> onSelectedChipsChanged;
  int amount = 100;
  TextEditingController amountController = new TextEditingController();
  List<PreviewPlay> previewPlays = new List();

  ChipsDialogState(this.raffle, this.columnCount, this.headerImages, this.chips,
      this.onSelectedChipsChanged) {
    amountController.text = amount.toString();
  }

  Color color = Colors.white;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(4.0),
      title: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Sorteo: ' + Conversions.getRaffleHour(raffle), style: TextStyle(fontSize: 15),),
              new Text(
                'Fichas: ' ,//+ LotoChip.getSelectedChipsCount(chips).toString(),
                style: new TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black, fontSize: 15),
              ),
            ],
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: headerImages)
        ],
      ),
      content: new Container(
        width: MediaQuery.of(context).size.width * .7,
        child: new GridView.count(
            crossAxisCount: columnCount,
            childAspectRatio: 1.0,
            padding: const EdgeInsets.all(2.0),
            mainAxisSpacing: 4.0,
            crossAxisSpacing: 4.0,
            children: chips.map((LotoChip chip) {
              return GestureDetector(
                child: Card(
                  color: chip.selected ? primaryColor : Color(0xffffffff),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          chip.number,
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                      Container(
                        width: 36,
                        height: 36,
                        decoration: new BoxDecoration(
                            image: DecorationImage(
                          image: AssetImage(removeDiacritics(chip.image)),
                        )),
                      ),
                      Text(
                        chip.description,
                        style: TextStyle(fontSize: 12),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  setState(() {
                    chip.selected = !chip.selected;
                  });
                },
              );
            }).toList()),
      ),
      actions: <Widget>[
        new IconButton(
            splashColor: Colors.red,
            icon: new Icon(
              Icons.close,
              color: Colors.red,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        new IconButton(
            splashColor: Colors.yellow,
            icon: new Icon(
              Icons.remove,
            ),
            onPressed: () {
              setState(() {
                if (amount > 0) {
                  amount = amount - 100;
                  amountController.text = amount.toString();
                }
              });
            }),
        Container(
            width: 40,
            child: TextField(
              textAlign: TextAlign.center,
              controller: amountController,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(), hintText: 'Monto'),
              onChanged: (String changed) {
                amount = int.parse(changed);
              },
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly
              ],
            )),
        new IconButton(
            splashColor: Colors.green,
            icon: new Icon(
              Icons.add,
            ),
            onPressed: () {
              setState(() {
                amount = amount + 100;
                amountController.text = amount.toString();
              });
            }),
        new IconButton(
            splashColor: Colors.blue,
            icon: new Icon(
              Icons.done,
              color: Colors.blue,
            ),
            onPressed: () {
              if (amount > 0 && LotoChip.getSelectedChips(chips).length > 0) {

                for (LotoChip c in LotoChip.getSelectedChips(chips)) {
                  PreviewPlay p = new PreviewPlay();
                  p.chip = c;
                  p.chip.selected = true;
                  p.amount = amount;
                  p.raffle = raffle;
                  previewPlays.add(p);
                }
                widget.onSelectedChipsChanged(previewPlays);
                Navigator.of(context).pop();
              } else if (amount <= 0){
                Fluttertoast.showToast(
                    msg: "Debe colocar un monto",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
              } else if (LotoChip.getSelectedChips(chips).length == 0) {
                Fluttertoast.showToast(
                    msg: "Debe seleccionar una ficha",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                );
              }
            })
      ],
    );
  }
}
