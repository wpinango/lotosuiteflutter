import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotosuiteplay/components/blinking_text.dart';
import 'package:lotosuiteplay/components/buttons/text_button.dart';
import 'package:lotosuiteplay/models/sell.dart';
import 'package:lotosuiteplay/models/sell_item.dart';
import 'package:lotosuiteplay/utils/conversions.dart';
import 'package:lotosuiteplay/utils/effective_sell.dart';
import 'package:lotosuiteplay/utils/tripleplay_available.dart';

class ConfirmPlaysDialog extends StatefulWidget {
  final ValueChanged<Sell> onConfirmPlay;
  final Sell sell;
  final int raffle;
  final bool isConfirm;

  const ConfirmPlaysDialog(
      {Key key, this.onConfirmPlay, this.raffle, this.sell, this.isConfirm})
      : super(key: key);

  @override
  ConfirmPlaysDialogState createState() => new ConfirmPlaysDialogState(
        onConfirmPlay: onConfirmPlay,
        raffle: raffle,
        sell: sell,
        isConfirm: isConfirm,
      );
}

class ConfirmPlaysDialogState extends State<ConfirmPlaysDialog> {
  ValueChanged<Sell> onConfirmPlay;
  Sell sell;
  TextEditingController amountController = new TextEditingController();
  int raffle;
  bool isConfirm;
  int triplePlayStatus;

  ConfirmPlaysDialogState(
      {this.onConfirmPlay, this.raffle, this.sell, this.isConfirm}) {
    //setState(() {
    triplePlayStatus = TriplePlayAvailable.isTicketAvailableTPWinner(
        sell.s);
    // });
  }

  Color color = Colors.white;

  String getTotalAmount() {
    int amount = 0;
    for (SellItem s in sell.s) {
      amount += s.amount;
    }
    return amount.toString();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(8.0),
      title: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          isConfirm
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text('Ticket: ' + sell.xSerial),
                    Row(
                      children: <Widget>[
                        Text(
                          EffectiveSell.checkEffectiveSell(sell)['sell']
                              .toString(),
                          style: TextStyle(color: Colors.green),
                        ),
                        Text('|'),
                        Text(
                          EffectiveSell.checkEffectiveSell(sell)['no_sell']
                              .toString(),
                          style: TextStyle(color: Colors.red),
                        ),
                      ],
                    )
                  ],
                )
              : Text('Jugadas ' +
                  sell.s.length.toString() +
                  ' | Total: ' +
                  getTotalAmount()),
          isConfirm
              ? null
              : BlinkingText(
                  triplePlayStatus == 0
                      ? 'Ticket valido para Triple Play'
                      : 'Ticket NO valido para Triple Play',
                  TextStyle(
                      color: triplePlayStatus == 0 ? Colors.green : Colors.red,
                      fontSize: 14))
        ].where((child) => child != null).toList(),
      ),
      content: new Container(
          width: MediaQuery.of(context).size.width * .7,
          child: ListView.builder(
            itemCount: sell.s.length,
            itemBuilder: (BuildContext context, int index) {
              String gameToUpperCase =
                   sell.s[index].name.substring(0, 1).toUpperCase() +
                      sell.s[index].name.substring(1);
              return ListTile(
                title: Text(gameToUpperCase +
                    " " +
                    Conversions.getRaffleHour(
                        sell.s[index].raffle['code']) +
                    " \n[ " +
                    sell.s[index].chip['number'] +
                    " ] " +
                    sell.s[index].description +
                    " x " +
                    sell.s[index].amount.toString()),
                trailing: isConfirm
                    ? sell.s[index].effectiveSell ==
                            sell.s[index].amount
                        ? Icon(
                            Icons.done,
                            color: Colors.green,
                          )
                        : Icon(Icons.close, color: Colors.red)
                    : null,
              );
            },
          )),
      actions: <Widget>[
        TextButton(
          buttonName: 'Cancelar',
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          buttonName: 'Aceptar',
          onPressed: () {
            onConfirmPlay(sell);
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
