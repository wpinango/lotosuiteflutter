import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:native_widgets/native_widgets.dart';

class MyDialog {
  static Future<bool> showNativePopUpWith2Buttons(
      BuildContext context,
      String title,
      String content,
      String negativeText,
      String positiveText) async {
    bool isPressed = await showDialog<bool>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => NativeDialog(
                title: title,
                content: content,
                actions: <NativeDialogAction>[
                  NativeDialogAction(
                    text: negativeText,
                    isDestructive: false,
                    // Set True to indicate with red accent
                    onPressed: () => Navigator.pop(
                        context, false), //Navigator.of(context).pop(false),
                  ),
                  NativeDialogAction(
                    text: positiveText,
                    isDestructive: false,
                    // Set True to indicate with red accent
                    onPressed: () => Navigator.pop(context, true),
                  ),
                ]));
    return isPressed;
  }

  static Future<bool> showNativeDialog(BuildContext context, String title,
      String content, String negativeText, String positiveText) async {
    bool isPressed = await showDialog<bool>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: new Text(title),
              content:
                  new Text("There was an error signing in. Please try again."),
              actions: <Widget>[
                new CupertinoDialogAction(
                    child: Text(positiveText),
                    isDestructiveAction: true,
                    onPressed: () {
                      Navigator.pop(context, true);
                    }),
                new CupertinoDialogAction(
                    child: Text(negativeText),
                    isDefaultAction: true,
                    onPressed: () {
                      Navigator.pop(context, false);
                    }),
              ],
            ));
    return isPressed;
  }
}
