import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lotosuiteplay/models/chip.dart';

class EditDialog extends StatefulWidget {
  final PreviewPlay previewPlay;
  final ValueChanged<PreviewPlay> onAmountEditedChanged;

  const EditDialog(
      {Key key,
        this.previewPlay,
        this.onAmountEditedChanged})
      : super(key: key);

  @override
  EditDialogState createState() => new EditDialogState(previewPlay);
}

class EditDialogState extends State<EditDialog> {
  ValueChanged<List<PreviewPlay>> onAmountEditedChanged;
  int amount;
  TextEditingController amountController = new TextEditingController();
  PreviewPlay previewPlay;

  EditDialogState(this.previewPlay) {
    amount = previewPlay.amount;
    amountController.text = amount.toString();
  }

  Color color = Colors.white;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(8.0),
      title: Column(
        children: <Widget>[
          Text('Editar Monto')
        ],
      ),
      actions: <Widget>[
        new IconButton(
            splashColor: Colors.red,
            icon: new Icon(
              Icons.close,
              color: Colors.red,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        new IconButton(
            splashColor: Colors.yellow,
            icon: new Icon(
              Icons.remove,
            ),
            onPressed: () {
              setState(() {
                if (amount > 0) {
                  amount = amount - 100;
                  amountController.text = amount.toString();
                }
              });
            }),
        Container(
            width: 50,
            child: TextField(
              controller: amountController,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(), hintText: 'Monto'),
              onChanged: (String changed) {
                amount = int.parse(changed);
              },
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly
              ],
            )),
        new IconButton(
            splashColor: Colors.green,
            icon: new Icon(
              Icons.add,
            ),
            onPressed: () {
              setState(() {
                amount = amount + 100;
                amountController.text = amount.toString();
              });
            }),
        new IconButton(
            splashColor: Colors.blue,
            icon: new Icon(
              Icons.done,
              color: Colors.blue,
            ),
            onPressed: () {
              if (amount > 0) {
                previewPlay.amount = amount;
                widget.onAmountEditedChanged(previewPlay);
                Navigator.of(context).pop();
              } else {

              }
            })
      ],
    );
  }
}