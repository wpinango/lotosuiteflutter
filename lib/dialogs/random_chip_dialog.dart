import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lotosuiteplay/models/chip.dart';
import 'package:lotosuiteplay/models/game_available.dart';
import 'package:lotosuiteplay/models/random.dart';

class RandomChipDialog extends StatefulWidget {
  final ValueChanged<List<PreviewPlay>> onRandomCountSelected;
  final List<Widget> headerImages;
  final int raffle;
  final List names;
  final GameAvailable gameAvailable;

  const RandomChipDialog(
      {Key key,
      this.onRandomCountSelected,
      this.headerImages,
      this.raffle,
      this.names,
      this.gameAvailable})
      : super(key: key);

  @override
  RandomChipDialogState createState() => new RandomChipDialogState(
      onRandomCountSelected: onRandomCountSelected,
      headerImages: headerImages,
      names: names,
      raffle: raffle,
      gameAvailable: gameAvailable);
}

class RandomChipDialogState extends State<RandomChipDialog> {
  ValueChanged<List<PreviewPlay>> onRandomCountSelected;
  int amount;
  List<Widget> headerImages;
  TextEditingController amountController = new TextEditingController();
  RandomCount randomCount = new RandomCount();
  List names = new List();
  int raffle;
  GameAvailable gameAvailable;

  RandomChipDialogState(
      {this.onRandomCountSelected,
      this.headerImages,
      this.names,
      this.raffle,
      this.gameAvailable}) {
    amount = 100;
    amountController.text = amount.toString();
    for (var name in names) {
      randomCount.chipsCount[name] = 3;
    }
  }

  Color color = Colors.white;

  List<Widget> getRandomWidget() {
    List<Widget> w = new List();
    for (var name in names) {
      w.add(
        Column(
          children: <Widget>[
            new IconButton(
                splashColor: Colors.green,
                icon: new Icon(
                  Icons.add,
                ),
                onPressed: () {
                  setState(() {
                    if (randomCount.chipsCount[name] < 36) {
                      randomCount.chipsCount[name]++;
                    }
                  });
                }),
            Text(randomCount.chipsCount[name].toString()),
            new IconButton(
                splashColor: Colors.red,
                icon: new Icon(
                  Icons.remove,
                ),
                onPressed: () {
                  setState(() {
                    if (randomCount.chipsCount[name] > 0) {
                      randomCount.chipsCount[name]--;
                    }
                  });
                }),
          ],
        ),
      );
    }
    return w;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(8.0),
      title: Column(
        children: <Widget>[
          Text('Selecciona aleatoriedad'),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: headerImages)
        ],
      ),
      content: new Container(
          height: MediaQuery.of(context).size.height / 5,
          width: MediaQuery.of(context).size.width * .7,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: getRandomWidget(),
          )),
      actions: <Widget>[
        new IconButton(
            splashColor: Colors.red,
            icon: new Icon(
              Icons.close,
              color: Colors.red,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        new IconButton(
            splashColor: Colors.yellow,
            icon: new Icon(
              Icons.remove,
            ),
            onPressed: () {
              setState(() {
                if (amount > 0) {
                  amount = amount - 100;
                  amountController.text = amount.toString();
                }
              });
            }),
        Container(
            width: 40,
            child: TextField(
              textAlign: TextAlign.center,
              controller: amountController,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(), hintText: 'Monto'),
              onChanged: (String changed) {
                amount = int.parse(changed);
              },
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly
              ],
            )),
        new IconButton(
            splashColor: Colors.green,
            icon: new Icon(
              Icons.add,
            ),
            onPressed: () {
              setState(() {
                amount = amount + 100;
                amountController.text = amount.toString();
              });
            }),
        new IconButton(
            splashColor: Colors.blue,
            icon: new Icon(
              Icons.done,
              color: Colors.blue,
            ),
            onPressed: () {
              if (amount > 0) {
                int size = 0;
                randomCount.chipsCount.forEach((key, value) {
                  if (value == 0) {
                    size++;
                  }
                });
                if (size == names.length) {
                  Fluttertoast.showToast(
                      msg: "Debe seleccionar una ficha",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else {
                  randomCount.amount = amount;
                  randomCount.raffle = raffle;
                  List<PreviewPlay> previewPlays = new List();

                  randomCount.chipsCount.forEach((key, value) {
                    for (int i = 0; i < value; i++) {
                      PreviewPlay previewPlay = new PreviewPlay();
                      previewPlay.chip =
                          RandomCount.getRandomChip(key, gameAvailable);
                      previewPlay.amount = randomCount.amount;
                      previewPlay.raffle = randomCount.raffle;
                      previewPlays.add(previewPlay);
                    }
                  });
                  widget.onRandomCountSelected(previewPlays);
                  Navigator.of(context).pop();
                }
              } else {
                Fluttertoast.showToast(
                    msg: "Debe colocar un monto",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
            })
      ],
    );
  }
}
