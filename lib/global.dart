import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lotosuiteplay/request/api_constants.dart';

import 'models/chip.dart';

class Global {
  static final String keySaf = "sgt-saf";
  static final String keyLot = "sgt-lot";
  static final String keyTiz = "sgt-tiz";
  static String token;
  static var userId;
  static var agencyId;
  static var currentGames;
  static var producerName;
  static var produceRating;
  static var features;
  static var userName;
  static var producerId;
  static var timestamp;
  static var raffles;
  static List<LotoChip> safList;
  static List<LotoChip> tizList;
  static List<LotoChip> lotList;
  static var codeNames;
  static var safariCodeName;
  static var tizanaCodeName;
  static var lotoCodeName;
  static String nick;
  static String userLogin;
  static double cash = 0;
  static int locked = 0;
  static int deferred = 0;
  static double promotional = 0;
  static var gameOptionId;

  static var okResponse = "OK";

  static void setData(var response) async {
    var r = (response);
    var games = r ['games'];
    Global.timestamp = r['timestamp'];
    //Global.userName = r['username'];
    Global.cash = r['cash'];
    Global.promotional = r['promotional'];
    Global.producerName = r['producerName'];
    Global.producerId = r['producerId'];
    Global.produceRating = r['producerRating'];
    Global.features = r['producerFeature'];
    Global.currentGames = games[0]['gameOptions'];
    Global.userId = r['user']['id'];
  }

}
