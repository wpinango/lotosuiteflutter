import 'package:lotosuiteplay/global.dart';

class LotoChip {
  String number;
  String description;
  var id;
  String image;
  bool selected = false;
  String codeName;
  String game;
  int gameId;
  String code;
  var gameOptionId;

  LotoChip({this.id, this.image, this.number, this.selected, this.description});

  factory LotoChip.fromJson(Map<String, dynamic> json) {
    return LotoChip(
        number: json['number'],
        description: json['description'],
        id: json['id'],
        image: json['image'],
        selected: json['selected']);
  }

  toJson() {
    return {
      'number': number,
      'description': description,
      'id': id,
      'image': image,
      'selected': selected,
      'codeName': codeName,
      'game': game
    };
  }

  static void sortChips(List chips) {
    List<LotoChip> arrayList = new List();
    Global.safList = new List();
    Global.tizList = new List();
    Global.lotList = new List();
    for (LotoChip c in chips) {
      if (c.number != "0" && c.number != "00" && c.number != "000") {
        arrayList.add(c);
      }
    }
    arrayList
        .sort((a, b) => int.parse(a.number).compareTo(int.parse(b.number)));
    int i = 0;
    for (LotoChip c in chips) {
      if (c.number == "0") {
        arrayList.insert(i, c);
        i++;
      }
    }
    for (LotoChip c in chips) {
      if (c.number == "00") {
        arrayList.insert(i, c);
        i++;
      }
    }
    for (LotoChip c in chips) {
      if (c.number == "000") {
        arrayList.insert(i, c);
        i++;
      }
    }
    for (LotoChip c in arrayList) {
      if (c.codeName == Global.keySaf) {
        Global.safList.add(c);
      }
    }
    for (LotoChip c in arrayList) {
      if (c.codeName == Global.keyTiz) {
        Global.tizList.add(c);
      }
    }
    for (LotoChip c in arrayList) {
      if (c.codeName == Global.keyLot) {
        Global.lotList.add(c);
      }
    }
    arrayList.clear();
    for (int i = 0; i < 36; i++) {
      if (Global.safList.isNotEmpty) {
        arrayList.add(Global.safList[i]);
      }
      if (Global.tizList.isNotEmpty) {
        arrayList.add(Global.tizList[i]);
      }
      if (Global.lotList.isNotEmpty) {
        arrayList.add(Global.lotList[i]);
      }
    }
    chips.clear();
    chips.addAll(arrayList);
  }

  static List<LotoChip> getSelectedChips(List<LotoChip> chips) {
    List<LotoChip> arrayList = new List();
    for (LotoChip c in chips) {
      if (c.selected) {
        arrayList.add(c);
      }
    }
    return arrayList;
  }

  static int getSelectedChipsCount(List<LotoChip> chips) {
    List<LotoChip> arrayList = new List();
    for (LotoChip c in chips) {
      if (c.selected) {
        arrayList.add(c);
      }
    }
    return arrayList.length;
  }

  static LotoChip getSelectedChip(List<LotoChip> chips) {
    LotoChip lotoChip = new LotoChip();
    for (LotoChip c in chips) {
      if (c.selected) {
        lotoChip = c;
      }
    }
    return lotoChip;
  }

  static dynamic getChipById(var id) {
    for (var item in Global.currentGames) {
      for (var c in item['chips']) {
        if (id == c['id']) {
          c['game'] = item['name'];
          return c;
        }
      }
    }
  }

  static dynamic getGameByChip(int id) {
    for (var item in Global.currentGames) {
      for (var c in item['chips']) {
        if (id == c['id']) {
          String name = item['name'].toString().substring(0, 1).toUpperCase() +
              item['name'].toString().substring(1);
          return name;
        }
      }
    }
  }
}

class PreviewPlay {
  LotoChip chip;
  int amount;
  int raffle;
  var raffleId;

  PreviewPlay({this.chip, this.amount, this.raffle});

  factory PreviewPlay.fromJson(Map<String, dynamic> json) {
    return PreviewPlay(
      chip: json['chip'],
      amount: json['amount'],
      raffle: json['raffle'],
    );
  }

  toJson() {
    return {
      'chip': chip,
      'amount': amount,
      'raffle': raffle,
    };
  }
}
