import 'package:flutter/cupertino.dart';
import 'package:lotosuiteplay/models/chip.dart';

class GameAvailable {
  int count = 0;
  Map chips;
  Map raffles;

  GameAvailable() {
    chips = new Map();
    raffles = new Map();
  }

  static List getAvailableGameNames(
      int raffle, GameAvailable gameAvailable) {
    //try {
      List names = new List();
      gameAvailable.raffles.forEach((key, value) {
        for (var v in value) {
          if (raffle == v['code']) {
            //if (key == 'safari play') {
            names.add(key.toString().split(" ")[0]);
            //}
          }
        }
      });
      return names;
    /*} catch (e) {
      e.toString();
    }*/
  }

  static List<Widget> getAvailableGameImage(
      int raffle, GameAvailable gameAvailable) {
    try {
      List names = new List();
      gameAvailable.raffles.forEach((key, value) {
        for (var v in value) {
          if (raffle == v['code']) {
            //if (key == 'safari play') {
            names.add(key.toString().split(" ")[0].toLowerCase() + '.png');
            //}
          }
        }
      });
      List<Widget> widget = new List();
      for (var name in names) {
        widget.add(Container(
          width: 60,
          height: 36,
          decoration: new BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/' + name),
          )),
        ));
      }
      return widget;
    } catch (e) {
      print(e.toString());
    }
  }

  static List<LotoChip> getAvailableChips(
      int raffle, GameAvailable gameAvailable) {
    try {
      List names = new List();
      List<LotoChip> chips = new List();
      gameAvailable.raffles.forEach((key, value) {
        for (var v in value) {
          if (raffle == v['code']) {
            //if (key == 'safari play') {
            names.add(key.toString());
            //}
          }
        }
      });
      gameAvailable.count = names.length;
      for (var name in names) {
        chips.addAll(gameAvailable.chips[name]);
      }
      LotoChip.sortChips(chips);
      return chips;
    } catch (e) {
      print(e.toString());
    }
  }
}
