import 'package:lotosuiteplay/global.dart';
import 'package:lotosuiteplay/models/chip.dart';

class Raffle {
  int id;
  int hour;

  static int findIdByRaffle(PreviewPlay previewPlay) {
    int id;
    for (var item in Global.currentGames) {
      for (var raffle in item['raffles']) {
        if (previewPlay.raffle == raffle['code']) {
          previewPlay.raffleId = raffle;
        }
      }
    }
    return id;
  }

  static dynamic findRaffleById(String id) {
    var r;
    for (var item in Global.currentGames) {
      for (var raffle in item['raffles']) {
        if (id == raffle['id'].toString()) {
          r = raffle;
          return r;
        }
      }
    }
    return r;
  }
}