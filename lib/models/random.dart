import 'dart:math';

import 'package:lotosuiteplay/models/chip.dart';
import 'package:lotosuiteplay/models/game_available.dart';

class RandomCount {
  Map<String, int> chipsCount = new Map();
  int amount;
  int raffle;

  static LotoChip getRandomChip(String name, GameAvailable gameAvailable) {
    LotoChip chip = new LotoChip();
    Random random = new Random();
    gameAvailable.chips.forEach((key, value) {
      for (LotoChip c in value) {
        if (c.game.split(" ")[0] == name) {
          chip = value[random.nextInt(value.length)];
          while (chip.selected) {
            chip = value[random.nextInt(value.length)];
          }
          chip.selected = true;
          break;
        } else {
          break;
        }
      }
    });
    print(chip);
    return chip;
  }
}
