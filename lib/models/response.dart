class Response {
  int timestamp;
  int status;
  String error;
  String exception;
  String message;
  String path;
  int cash;
  int promotional;
  String username;
  int locked;
  int deferred;
  String hashResult;
  String hashBalance;
}