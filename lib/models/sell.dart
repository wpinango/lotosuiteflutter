import 'package:lotosuiteplay/models/sell_item.dart';
import 'package:lotosuiteplay/models/tp_helper.dart';
import 'package:lotosuiteplay/models/tp_helper_item.dart';

class Sell {
  var userId;
  String xSerial;
  Map<int, TPHelper> sellItems;
  List<SellItem> s;
  var totalAmount;
  var cash;
  var promotional;

  Sell(
      {this.xSerial,
      this.userId,
      this.sellItems,
      this.promotional,
      this.totalAmount,
      this.cash,
      this.s}) {
    s = new List();
    sellItems = new Map();
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = new Map();
    var sellItemsToJson = new Map();
    try {
      sellItems.forEach((key, value) {
        sellItemsToJson[key.toString()] = value;
      });
    } catch (e) {
      e.toString();
    }
    data['userId'] = userId;
    data['xSerial'] = xSerial;
    data['sellItems'] = sellItemsToJson;
    data['totalAmount'] = totalAmount;
    return data;
  }
}
