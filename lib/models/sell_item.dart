class SellItem {
  var gameOptionId;
  var raffle;
  var chip;
  var amount;
  double effectiveSell;
  String codeName;
  String description;
  bool selected;
  String name;


  toJson() {
    return {
      'chip': chip,
      'amount': amount,
      'effectiveSell': effectiveSell,
      'codeName': codeName,
      'description': description,
    };
  }

}
