import 'dart:convert';

import 'package:lotosuiteplay/models/sell_item.dart';
import 'package:lotosuiteplay/models/tp_helper_item.dart';

class TPHelper {

  Map<int, List<SellItem>> tpHelpers;

  TPHelper() {
    tpHelpers = new Map();
  }

  void addTPHelper(int raffle, List<SellItem> s) {
    List<SellItem> list = tpHelpers[raffle];
    if (list == null) {
      list = new List();
      tpHelpers.putIfAbsent(raffle, () => list);
    }
    list.addAll(s);
  }

  toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tpHelpers != null) {
      tpHelpers.forEach((key, value) {
        data[key.toString()] = (value);
      });
    }
    return data;
  }
}