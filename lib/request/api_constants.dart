
class Api {
  static var timeout = const Duration(seconds: 80);
  static final String keyToken = "x-session-token";
  static final String keyUserId = "x-identity";
  static final String keyAgencyId = "x-agency-id";
  static final String keySecurity = "x-security";
  static final String keyProducerId = "x-pid";
  static final String keyAuth = "Authorization";
  static final String keySchema = "x-schema";
  static final String keyNew = "x-new-key";
  static final String keyMobile = "movilx";
  static final String keyFBToken = "x-msg-token";
  static final String keyTimestamp = "x-ts-sync";
  static final String keyHashBalance = "x-hash-balance";
  static final String keyHashResult = "x-hash-result";

  //static String host = "http://api-ve.lotosuite.com:8090";
  static String host = 'http://46.101.158.43:8080';

  static String urlOneTicket = host + "/mobile/getOneTicket";
  //static String urlAllTicket = host + "/mobile/getAllTickets";
  static String urlAllTicket = host + '/private/getAllTickets';
  //static String urlSell = host + "/mobile/sell";
  static String urlSell = host + "/private/sell";
  //static String urlLogin = host + "/mobile/doAnonymousLogin";
  static String urlLogin = host + "/public/doAnonymousLogin";
  static String urlSynchronization = host + '/private/synchronization';
  static String urlGetResult = host + '/mobile/resultsMobile';
  static String urlDiscardTicket = host + '/mobile/discardItemsGroup';
  static String urlRequestCash = host + '/mobile/requestCash';
  //static String urlRequestBalance = host + '/mobile/getBalanceHistory';
  static String urlRequestBalance = host + "/private/getBalanceHistory";
  static String urlGetProducer = host + '/mobile/getProducerList';
  static String urlRegisterUser = host + '/mobile/registerNewUser';
  static String urlCloseToken = host + '/mobile/destroySession';
  static String urlGetPendingRecharge = host + '/mobile/getPendingRecharges';
  static String urlGetPayment = host + '/mobile/requestPayment';
  static String urlGetPendingPayment = host + '/mobile/getPaymentPending';
  static String urlDeletePendingTransaction =
      host + '/mobile/deletePendingTransaction';
  static String urlGetProducerRating = host + '/mobile/getProducerRating';
  static String urlFindUser = host + '/mobile/findUser';
  static String urlSenTransfer = host + '/mobile/transfer';
  static String urlRateProducer = host + '/mobile/rateProducer';
  static String urlGetProducerFeatures = host + '/mobile/getProducerFeatures';
  static String urlChangeProducer = host + '/mobile/changeProducer';
  static String urlGetBanks = host + '/mobile/getBanks';
  static String urlRegisterAccount = host + '/mobile/registerAccountNumber';
  static String urlGetPlayerAccount = host + '/mobile/getPlayerAccounts';
  static String urlUpdatePlayerAccount = host + '/mobile/updatePlayerAccounts';
  static String urlDeletePlayerAccount = host + '/mobile/deletePlayerAccounts';
  static String urlGetProducerInformation = host + '/mobile/getUserInformation';
  static String urlUpdateUserProfile = host + '/mobile/updateUserProfile';
  static String urlUpdatePassword = host + '/mobile/updatePassword';
  static String urlRequestPassword = host + '/mobile/requestAccountUnlock';
  static String urlUpdateFBToken = host + '/mobile/updateFBaseToken';
  static String urlGetTutorialMedia = host + '/mobile/getVideos';
  static String urlGet3PWinners = host + '/mobile/getTriplePlayData';
  static String urlGetStatistics = host + '/mobile/resultsMobileStatistics';
  static String urlGet3PHistory = host + '/mobile/getTriplePlayHistory';
}
