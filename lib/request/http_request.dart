

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';

import 'package:lotosuiteplay/request/api_constants.dart';

class HttpRequest {
  static Future<Response> login(String username, String pass) async {
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      Api.keyFBToken : '1'
    };

    Map<String, String> body = {
      'username' : username.trim(),
      'password' : pass,
    };
    Response res =
        await post(Api.urlLogin, headers: headers, body: body).timeout(Api.timeout);
    return res;
  }

  static Future<Response> postRequestWithAuth(
      String url, Map<String, String> headers, dynamic body) async {
    final store = new FlutterSecureStorage();
    String token = await store.read(key: 'token');
    Map<String, String> h = {
      'Content-Type': 'application/json',
      Api.keyAuth: token,
    };
    headers.addAll(h);
    Response res =
        await post(url, headers: headers, body: body).timeout(Api.timeout);

    if (res.statusCode == 200) {
      return res;
    } else {
      print((res.body));
      return null;
    }
  }

  static Future<Response> getRequestWithAuth(
      String url, Map<String, String> headers) async {
    Map<String, String> h = {
      'Content-Type': 'application/json',
    };
    headers.addAll(h);
    Response res = await get(url, headers: headers).timeout(Api.timeout);
    print(res.statusCode);
    if (res.statusCode == 200) {
      return res;
    } else {
      return null;
    }
  }

  static Future<Response> getRequest(
      dynamic uri, Map<String, String> headers) async {
    try {
      final store = new FlutterSecureStorage();
      String token = await store.read(key: 'token');
      Map<String, String> h = {
        Api.keyAuth: token,
      };
      headers.addAll(h);
      Response res = await get(uri, headers: headers).timeout(Api.timeout);
      print(res.statusCode);
      return res;
    } catch(e) {
      return null;
    }
  }
}
