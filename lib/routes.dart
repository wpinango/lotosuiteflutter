import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotosuiteplay/screens/login_screen/login_screen.dart';
import 'package:lotosuiteplay/screens/main_screen/main_screen.dart';
import 'package:lotosuiteplay/screens/signup_screen/signup_screen.dart';
import 'package:lotosuiteplay/screens/splash_screen.dart';
import 'package:lotosuiteplay/theme.dart';

class Routes {
  var routes = <String, WidgetBuilder> {
    "/Login": (BuildContext context) => new LoginScreen(),
    "/Main": (BuildContext context) => new MainScreen(),
    '/SingUp': (BuildContext context) => new SignUpScreen(),
  };

  Routes() {
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Lotosuite Play",
      home: new FirstScreen(),
      routes: routes,
      theme: ThemeData(
        primaryColor: primaryColor,
        bottomAppBarColor: secondaryColor,

        //primaryColor: primaryColor,
      ),
    ));
  }

  static replacementScreen(BuildContext context, String routeName) {
    Navigator.pushReplacementNamed(context, routeName);
  }

  static showModalScreen(BuildContext context, String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }
}