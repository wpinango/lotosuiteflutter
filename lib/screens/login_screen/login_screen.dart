
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:lotosuiteplay/components/buttons/rounded_button.dart';
import 'package:lotosuiteplay/components/buttons/text_button.dart';
import 'package:lotosuiteplay/components/text_fields/input_field.dart';
import 'package:lotosuiteplay/components/text_fields/password_input_field.dart';
import 'package:lotosuiteplay/config/assest.dart';
import 'package:lotosuiteplay/screens/login_screen/style.dart';
import 'package:lotosuiteplay/services/validations.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../../request/api_constants.dart';
import '../../global.dart';
import '../../request/http_request.dart';
import '../../routes.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  Validations validations = new Validations();
  bool isInAsyncCall = false;
  var timeout = const Duration(seconds: 10);
  bool isHidePass = true;
  bool isLogin = false;
  String pass, userName;
  SharedPreferences sharedPreferences;
  TextEditingController userLoginController = new TextEditingController();

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString('userName') != null) {
      userLoginController.text =
          (sharedPreferences.getString('userName').replaceAll("M", ""));
    }
  }

  onPressed(String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void doLogin() async {
    showProgressDialog();
    var response = await HttpRequest.login(userName, pass);
    handleLoginResponse(response);
  }

  void handleLoginResponse(final response) async {
    cancelProgressDialog();
    if (response.statusCode == 200) {
      if (response.headers[Api.keyToken] != null) {
        Global.token = response.headers[Api.keyToken];
        final store = new FlutterSecureStorage();
        await store.write(key: 'token', value: Global.token);
        sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.setBool('login', true);
        sharedPreferences.setString('userName', userName);
        Routes.replacementScreen(context, '/Main');

      }
    } else {
      var res = json.decode(response.body);
      showInSnackBar(res['message']);
    }
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true;
      showInSnackBar('please resolve the errors first');
    } else {
      form.save();
      print("buttonClicked");
      doLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: new Container(
          decoration: new BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(Assets.backgroundLogin),
                  fit: BoxFit.cover,
                )),
          child: SingleChildScrollView(
            child: Container(
              height: screenSize.height,
              child: new Column(
                children: <Widget>[
                  new Container(
                    height: screenSize.height / 1.2,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(20),
                        ),
                        new Image.asset(
                          Assets.lotoplay,
                          height: screenSize.height / 4,
                        ),
                        Padding(
                          padding: EdgeInsets.all(20),
                        ),
                        Container(
                          child: Card(
                            color: Color(0xffe1e1e1),
                            margin: EdgeInsets.all(16),
                            child: Container(
                              padding: EdgeInsets.all(8),
                              child: new Form(
                                key: formKey,
                                autovalidate: autoValidate,
                                child: new Column(
                                  children: <Widget>[
                                    new InputField(
                                        hintText: "Usuario",
                                        controller: userLoginController,
                                        obscureText: false,
                                        textInputType:
                                            TextInputType.emailAddress,
                                        textCapitalization:
                                            TextCapitalization.none,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.person,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 8.0,
                                        validateFunction:
                                            validations.validateEmail,
                                        onSaved: (String user) {
                                          userName = user;
                                        }),
                                    new PasswordInputField(
                                        hintText: "Contraseña",
                                        obscureText: isHidePass,
                                        textInputType: TextInputType.text,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.lock_open,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 8.0,
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              isHidePass = !isHidePass;
                                            });
                                          },
                                          child: Icon(!isHidePass
                                              ? Icons.visibility
                                              : Icons.visibility_off),
                                        ),
                                        validateFunction:
                                            validations.validatePassword,
                                        onSaved: (String password) {
                                          pass = password;
                                        }),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        new TextButton(
                                          buttonName: "Registro",
                                          onPressed: () {
                                            Routes.showModalScreen(context, '/SingUp');
                                          },
                                        ),
                                        new TextButton(
                                          buttonName: "Recuperar Contraseña",
                                          onPressed: () => onPressed(""),
                                        ),
                                      ],
                                    ),
                                    new RoundedButton(
                                      buttonName: "Entrar",
                                      onTap: handleFormSubmitted,
                                      width: screenSize.width / 1.5,
                                      height: screenSize.height / 15,
                                      bottomMargin: 10.0,
                                      borderWidth: 0.0,
                                      buttonColor: Colors.green,
                                    ),

                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'Derechos Revervados Azar Technologies, C.A.',
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              'www.azartechnologies.com',
                              textAlign: TextAlign.center,
                            ),
                          ],
                        )),
                  )
                ],
              ),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: kIsWeb ? CircularProgressIndicator():NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: true,
    );
  }
}
