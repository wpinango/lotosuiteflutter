import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotosuiteplay/dialogs/dialog.dart';
import 'package:lotosuiteplay/tabs/balance_tab.dart';
import 'package:lotosuiteplay/tabs/notifications_tab.dart';
import 'package:lotosuiteplay/tabs/plays_tab.dart';
import 'package:lotosuiteplay/tabs/result_tab.dart';
import 'package:lotosuiteplay/tabs/statistics_tab.dart';
import 'package:lotosuiteplay/tabs/tickets_tab.dart';
import 'package:lotosuiteplay/tabs/tripleplay_tab.dart';
import 'package:lotosuiteplay/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../routes.dart';

class MainScreen extends StatefulWidget {
  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  int selectedIndex = 0;
  bool isInAsyncCall = false;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  PlaysTab playsTab = new PlaysTab();
  TicketsTab ticketsTab = new TicketsTab();
  ResultTab resultTab = new ResultTab();
  TriplePlayTab triplePlayTab = new TriplePlayTab();
  NotificationTab notificationTab = new NotificationTab();
  StatisticsTab statisticsTab = new StatisticsTab();
  BalanceTab balanceTab = new BalanceTab();

  @override
  void initState() {
    super.initState();
  }

  static void showLogoutDialog(BuildContext context) async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context,
        'Problema de autorizacion',
        'Debe iniciar sesion nuevamente',
        'Cancelar',
        'Aceptar')) {
      logoutApp(context);
    }
  }

  static void logoutApp(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool('login', false);
    Routes.replacementScreen(context, '/Login');
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return DefaultTabController(
      length: 7,
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(
            "Lotosuite Play",
          ),
          backgroundColor: primaryColor,
          actions: <Widget>[],
          bottom: PreferredSize(
            preferredSize: size / 20,
            child: TabBar(
              isScrollable: true,
              tabs: [
                Tab(
                  text: 'Jugadas',
                ),
                Tab(
                  text: 'Mis Jugadas',
                ),
                Tab(
                  text: 'Resultados',
                ),
                Tab(text: 'Triple Play'),
                Tab(
                  text: 'Mi balance',
                ),
                Tab(
                  text: 'Estadisticas',
                ),
                Tab(
                  text: 'Notificaciones',
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            playsTab,
            ticketsTab,
            resultTab,
            triplePlayTab,
            balanceTab,
            statisticsTab,
            notificationTab
          ],
        ),
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 32),
              child: Column(
                children: <Widget>[
                  new Container(
                    height: 120.0,
                    child: new DrawerHeader(
                      padding: new EdgeInsets.all(0.0),
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/lotoplay3.png'),
                        ),
                        color: new Color(0xFFECEFF1),
                      ),
                    ),
                  ),
                  new ListTile(
                      leading: new Icon(Icons.games),
                      title: new Text('Juegos Disponibles'),
                      onTap: () {
                        Navigator.pop(context);
                        //onPressed(context,'/NeoBank');
                      }),
                  new Divider(),
                  new ListTile(
                      leading: new Icon(Icons.person),
                      title: new Text('Usuario'),
                      onTap: () {
                        Navigator.pop(context);
                        //onPressed(context,'/NeoBank');
                      }),
                  new ListTile(
                      leading: new Icon(Icons.lock),
                      title: new Text('Restriccion de Juego'),
                      onTap: () {
                        Navigator.pop(context);
                        //onPressed(context,'/Config');
                      }),
                  new ListTile(
                      leading: new Icon(Icons.exit_to_app),
                      title: new Text('Cerrar Sesion'),
                      onTap: () {
                        Navigator.pop(context);
                        logout();
                        //showShareApps();
                      }),
                  new Divider(),
                  new ListTile(
                      leading: new Icon(Icons.attach_money),
                      title: new Text('Notificar Recargas'),
                      onTap: () {
                        Navigator.pop(context);
                        //showNativePopUp(context);
                        //showSignOutDialog();
                      }),
                  new ListTile(
                      leading: new Icon(Icons.exit_to_app),
                      title: new Text('Solicitar Pagos'),
                      onTap: () {
                        Navigator.pop(context);
                        //showNativePopUp(context);
                        //showSignOutDialog();
                      }),
                  new Divider(),
                  new ListTile(
                      leading: new Icon(Icons.work),
                      title: new Text('Informacion del asesor'),
                      onTap: () {
                        Navigator.pop(context);
                        //showNativePopUp(context);
                        //showSignOutDialog();
                      }),
                  new Divider(),
                  new ListTile(
                      leading: new Icon(Icons.share),
                      title: new Text('Compartir'),
                      onTap: () {
                        Navigator.pop(context);
                        //showNativePopUp(context);
                        //showSignOutDialog();
                      }),
                  new ListTile(
                      leading: new Icon(Icons.star),
                      title: new Text('Calificar'),
                      onTap: () {
                        Navigator.pop(context);
                        //showNativePopUp(context);
                        //showSignOutDialog();
                      }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  logout() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context, 'Alerta', 'Desea cerrar sesion?', 'Cerrar', 'Aceptar')) {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setBool('login', false);
      Routes.replacementScreen(context, '/Login');
    }
  }
}
