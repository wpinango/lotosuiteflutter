import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotosuiteplay/components/buttons/rounded_button.dart';
import 'package:lotosuiteplay/components/buttons/text_button.dart';
import 'package:lotosuiteplay/components/text_fields/input_field.dart';
import 'package:lotosuiteplay/components/text_fields/password_input_field.dart';
import 'package:lotosuiteplay/config/assest.dart';
import 'package:lotosuiteplay/request/http_request.dart';
import 'package:lotosuiteplay/screens/login_screen/style.dart';
import 'package:lotosuiteplay/services/validations.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key key}) : super(key: key);

  @override
  SignUpScreenState createState() => new SignUpScreenState();
}

class SignUpScreenState extends State<SignUpScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  Validations validations = new Validations();
  bool isInAsyncCall = false;
  var timeout = const Duration(seconds: 10);
  bool isHidePass = true;
  bool isLogin = false;
  String pass, userName;
  TextEditingController userLoginController = new TextEditingController();

  @override
  initState() {
    super.initState();
    initVars();
  }

  void initVars() async {}

  onPressed(String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void doLogin() async {
    showProgressDialog();
    var response = await HttpRequest.login(userName, pass);
    handleLoginResponse(response);
  }

  void handleLoginResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response.body);
      if (map['message'] != "" && map['error'] == '') {
        try {
          //Routes.replacementScreen(context, '/Main');
        } catch (e) {
          print(e.toString());
        }
      } else if (map['message'] != '' && map['error'] != '') {
        showInSnackBar(map['message']);
      } else {
        showInSnackBar('Algo salio mal');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Ckech your internet connection");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true;
      showInSnackBar('please resolve the errors first');
    } else {
      form.save();
      print("buttonClicked");
      doLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: new Container(
          decoration: new BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(Assets.backgroundLogin),
                  fit: BoxFit.cover)),
          child: new Column(
            children: <Widget>[
              new Container(
                height: screenSize.height / 1.2,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    new Image.asset(
                      Assets.lotoplay,
                      height: screenSize.width / 2,
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Container(
                      child: Card(
                        color: Color(0xffe1e1e1),
                        margin: EdgeInsets.all(16),
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: new Form(
                            key: formKey,
                            autovalidate: autoValidate,
                            child: new Column(
                              children: <Widget>[
                                new InputField(
                                    hintText: "Cedula",
                                    obscureText: false,
                                    textInputType: TextInputType.emailAddress,
                                    textCapitalization: TextCapitalization.none,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.contacts,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    validateFunction: validations.validateEmail,
                                    onSaved: (String user) {
                                      //userName = 'M' + user;
                                    }),
                                Row(
                                  children: <Widget>[
                                    new InputField(
                                        hintText: "Primer Nombre",
                                        obscureText: false,
                                        textInputType:
                                            TextInputType.emailAddress,
                                        textCapitalization:
                                            TextCapitalization.none,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.person,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 8.0,
                                        validateFunction:
                                            validations.validateEmail,
                                        onSaved: (String user) {
                                          //userName = 'M' + user;
                                        }),
                                    new InputField(
                                        hintText: "Primer Apellido",
                                        obscureText: false,
                                        textInputType:
                                            TextInputType.emailAddress,
                                        textCapitalization:
                                            TextCapitalization.none,
                                        textFieldColor: textFieldColor,
                                        icon: Icons.person,
                                        iconColor: const Color(0xFF386080),
                                        bottomMargin: 8.0,
                                        validateFunction:
                                            validations.validateEmail,
                                        onSaved: (String user) {
                                          //userName = 'M' + user;
                                        }),
                                  ],
                                ),
                                new InputField(
                                    hintText: "Fecha de Nacimiento",
                                    controller: userLoginController,
                                    obscureText: false,
                                    textInputType: TextInputType.emailAddress,
                                    textCapitalization: TextCapitalization.none,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.calendar_today,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    validateFunction: validations.validateEmail,
                                    onSaved: (String user) {
                                      //userName = 'M' + user;
                                    }),
                                new InputField(
                                    hintText: "Alias",
                                    obscureText: false,
                                    textInputType: TextInputType.emailAddress,
                                    textCapitalization: TextCapitalization.none,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.person,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    validateFunction: validations.validateEmail,
                                    onSaved: (String user) {
                                      //userName = 'M' + user;
                                    }),
                                new InputField(
                                    hintText: "Correo",
                                    obscureText: false,
                                    textInputType: TextInputType.emailAddress,
                                    textCapitalization: TextCapitalization.none,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.email,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    validateFunction: validations.validateEmail,
                                    onSaved: (String user) {
                                      //userName = 'M' + user;
                                    }),
                                new PasswordInputField(
                                    hintText: "Contraseña",
                                    obscureText: isHidePass,
                                    textInputType: TextInputType.text,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.lock_open,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          isHidePass = !isHidePass;
                                        });
                                      },
                                      child: Icon(!isHidePass
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                    ),
                                    validateFunction:
                                        validations.validatePassword,
                                    onSaved: (String password) {
                                      pass = password;
                                    }),
                                new PasswordInputField(
                                    hintText: "Repetir contraseña",
                                    obscureText: isHidePass,
                                    textInputType: TextInputType.text,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.lock_open,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          isHidePass = !isHidePass;
                                        });
                                      },
                                      child: Icon(!isHidePass
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                    ),
                                    validateFunction:
                                        validations.validatePassword,
                                    onSaved: (String password) {
                                      pass = password;
                                    }),
                                new InputField(
                                    hintText: "Asesor",
                                    obscureText: false,
                                    textInputType: TextInputType.emailAddress,
                                    textCapitalization: TextCapitalization.none,
                                    textFieldColor: textFieldColor,
                                    icon: Icons.work,
                                    iconColor: const Color(0xFF386080),
                                    bottomMargin: 8.0,
                                    validateFunction: validations.validateEmail,
                                    onSaved: (String user) {
                                      //userName = 'M' + user;
                                    }),
                                new TextButton(
                                  buttonName: "Ver Terminos y Condiciones",
                                  onPressed: () => onPressed(""),
                                ),
                                new RoundedButton(
                                  buttonName: "Registrar",
                                  onTap: handleFormSubmitted,
                                  width: screenSize.width / 1.5,
                                  height: screenSize.height / 15,
                                  bottomMargin: 10.0,
                                  borderWidth: 0.0,
                                  buttonColor: Colors.green,
                                ),
                                new TextButton(
                                  buttonName: "Ya poseo cuenta",
                                  onPressed: () => onPressed(""),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'Derechos Revervados Azar Technologies, C.A.',
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          'www.azartechnologies.com',
                          textAlign: TextAlign.center,
                        ),
                      ],
                    )),
              )
            ],
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }
}
