import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lotosuiteplay/config/assest.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

import 'login_screen/login_screen.dart';
import 'main_screen/main_screen.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key key}) : super(key: key);

  @override
  FirstScreenState createState() => new FirstScreenState();
}

class FirstScreenState extends State<FirstScreen> {
  bool isLogin = false;

  @override
  void initState() {
    checkUserLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 4,
      navigateAfterSeconds: isLogin ? new MainScreen() : new LoginScreen(),
      image:new Image.asset(Assets.lotoplay),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      onClick: () => print("Lotosuite"),
      loaderColor: Colors.black87,
    );
  }

  void checkUserLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('login') != null) {
      setState(() {
        isLogin = prefs.getBool('login');
      });
    }
  }
}