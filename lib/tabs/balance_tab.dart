import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:lotosuiteplay/components/amount_text_field.dart';
import 'package:lotosuiteplay/request/api_constants.dart';
import 'package:lotosuiteplay/request/http_request.dart';

import '../global.dart';

class BalanceTab extends StatefulWidget {
  @override
  BalanceTabState createState() => BalanceTabState();
}

class BalanceTabState extends State<BalanceTab>
    with AutomaticKeepAliveClientMixin<BalanceTab> {
  int starDate, endDate;
  bool isInAsyncCall = false;
  Map decodeMessage = new Map();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    requestTodayBalance();
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  requestTodayBalance() {
    DateTime now = new DateTime.now();
    starDate = new DateTime(now.year, now.month, now.day, 0, 0, 0)
        .millisecondsSinceEpoch;
    endDate = new DateTime(
            now.year, now.month, now.day, now.hour, now.minute + 1, now.second)
        .millisecondsSinceEpoch;
    requestBalance();
  }

  requestBalance() async {
    Map map = {'startDate': starDate, 'endDate': endDate};
    showProgressDialog();
    var response = await HttpRequest.postRequestWithAuth(
        Api.urlRequestBalance, new Map(), json.encode(map));
    handleBalanceResponse(response);
  }

  handleBalanceResponse(final response) async {
    cancelProgressDialog();
    if (response != '' && response != null) {
      Map<String, dynamic> map = json.decode(response.body);
      if (map['message'] != "" && map['error'] == 'OK') {
        decodeMessage = map['message'];
        setState(() {});
      } else if (map['error'] != '') {
        /*if (map['error'] == 'no-content') {
          showInSnackBar('No se encontro tickets para esta fecha');
          setState(() {

          });
        } else if (map['error'] == ResponseConstants.authProblem) {
          MainScreenState.showLogoutDialog(context);
        }*/
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
          height: size.height,
          child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AmountTextField("Saldo Disponible", Global.cash.toString(),
                TextStyle(color: Colors.green, fontSize: 22), size),
            AmountTextField("Saldo Bloqueado", "0",
                TextStyle(color: Colors.deepOrangeAccent, fontSize: 22), size)
          ].where((child) => child != null).toList(),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AmountTextField("Saldo Especial", "0",
                TextStyle(color: Colors.green, fontSize: 22), size),
            AmountTextField("Saldo Diferido", "0",
                TextStyle(color: Colors.deepOrangeAccent, fontSize: 22), size)
          ].where((child) => child != null).toList(),
        ),
        Padding(padding: EdgeInsets.all(12),),
        createTable()
      ].where((child) => child != null).toList())),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.refresh, color: Colors.white),
                onPressed: () {
                  requestTodayBalance();
                },
              ),
              /*IconButton(
                icon: Icon(Icons.cancel, color: Colors.white),
                onPressed: () {
                  discardTicket();
                },
              ),*/
              IconButton(
                icon: Icon(Icons.search, color: Colors.white),
                onPressed: () {
                  //selectDate();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget createTable() {
    List<Widget> rows = new List();
    rows.add(Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
      Center(child: Text('Fecha',style: TextStyle(fontWeight: FontWeight.bold),)),
      Center(
        child: Text(
          'Monto',style: TextStyle(fontWeight: FontWeight.bold)
        ),
      ),
      Center(child: Text('Balance', style: TextStyle(fontWeight: FontWeight.bold))),
      Center(child: Text('Referencia', style: TextStyle(fontWeight: FontWeight.bold))),
    ],));
    rows.add(Padding(padding: EdgeInsets.all(2),));
    if (decodeMessage.length > 0) {
      for (var item in decodeMessage['playerTransactions']) {
        rows.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
          Center(child: Text(item['operationDate'])),
          Center(
              child: Text(
                item['amount'].toString(),
              ),
            ),
          Center(child: Text(item['balance'].toString())),
          Center(child: Text(item['reference'])),
        ],));
        rows.add(Padding(padding: EdgeInsets.all(4),));
      }
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: rows,);
    /*List<TableRow> rows = [];
    rows.add(TableRow(children: [
      TableCell(
          child: Center(
              child: Text(
        'Fecha',
        style: TextStyle(fontWeight: FontWeight.bold),
      ))),
      TableCell(
        child: Center(
            child:
                Text('Monto', style: TextStyle(fontWeight: FontWeight.bold))),
      ),
      TableCell(
          child: Center(
              child: Text('Balance',
                  style: TextStyle(fontWeight: FontWeight.bold)))),
      TableCell(
          child: Center(
              child: Text('Referencia',
                  style: TextStyle(fontWeight: FontWeight.bold)))),
    ]));
    if (decodeMessage.length > 0) {
      for (var item in decodeMessage['playerTransactions']) {
        rows.add(TableRow(children: [
          TableCell(child: Center(child: Text(item['operationDate']))),
          TableCell(
            child: Center(
              child: Text(
                item['amount'].toString(),
              ),
            ),
          ),
          TableCell(child: Center(child: Text(item['balance'].toString()))),
          TableCell(child: Center(child: Text(item['reference']))),
        ]));
      }
    }
    return Table(children: rows);*/
  }
}
