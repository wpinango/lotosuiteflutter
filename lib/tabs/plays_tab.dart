import 'dart:convert';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:lotosuiteplay/dialogs/confirm_plays_dialog.dart';
import 'package:lotosuiteplay/dialogs/chips_dialog.dart';
import 'package:lotosuiteplay/dialogs/edit_dialog.dart';
import 'package:lotosuiteplay/dialogs/random_chip_dialog.dart';
import 'package:lotosuiteplay/models/chip.dart';
import 'package:lotosuiteplay/models/game_available.dart';
import 'package:lotosuiteplay/models/raffle.dart';
import 'package:lotosuiteplay/models/sell.dart';
import 'package:lotosuiteplay/models/sell_item.dart';
import 'package:lotosuiteplay/models/tp_helper.dart';
import 'package:lotosuiteplay/request/api_constants.dart';
import 'package:lotosuiteplay/request/http_request.dart';
import 'package:lotosuiteplay/request/response_constants.dart';
import 'package:lotosuiteplay/screens/main_screen/main_screen.dart';
import 'package:lotosuiteplay/utils/conversions.dart';
import 'package:lotosuiteplay/utils/time.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../global.dart';

class PlaysTab extends StatefulWidget {
  const PlaysTab({
    Key key,
  }) : super(key: key);

  @override
  PlaysTabState createState() => PlaysTabState();
}

class PlaysTabState extends State<PlaysTab>
    with AutomaticKeepAliveClientMixin<PlaysTab> {
  List raffles = new List();
  List<PreviewPlay> previewPlays = new List();
  int sellAmount = 0;
  GameAvailable gameAvailable;
  final moneyFormatter = new NumberFormat("#,###", "de_DE");
  Map<String, PreviewPlay> previewMapPlays = new Map();
  bool isRandom = false;
  bool isInAsyncCall = false;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PlaysTabState();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    checkData();
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void checkData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String data = sharedPreferences.getString('allData');
    if (Global.currentGames == null) {
      if (data == null) {
        synchronization();
      } else {
        Global.setData(json.decode(data));
        synchronization();
        getData();
      }
    } else {
      getData();
      synchronization();
    }
  }

  void synchronization() async {
    showProgressDialog();
    int time = 1585853380;
    var uri = Uri.parse(Api.urlSynchronization);
    uri = uri.replace(query: 'timestampSyncronization=$time');
    var response = await HttpRequest.getRequest(uri, new Map());
    handleSynchronizationResponse(response);
  }

  void handleSynchronizationResponse(var response) async {
    cancelProgressDialog();
    try {
      if (response.statusCode == 200 || response.statusCode == 202) {
        if (response != null && response != '') {
          var sync = json.decode(utf8.decode(response.bodyBytes));
          var r = sync['message'];
          r.putIfAbsent('timestamp', () => sync['timestamp']);
          if (r['synchronization']) {
            SharedPreferences sharedPreferences =
                await SharedPreferences.getInstance();
            sharedPreferences.setString('allData', json.encode(r));
            Global.setData((r));
            getData();
            setState(() {});
          }
        }
      } else {
        var res = json.decode(response.body);
        showInSnackBar(res['message']);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void getData() async {
    List raffles = new List();
    gameAvailable = new GameAvailable();
    var syncDate = new DateTime.fromMillisecondsSinceEpoch(Global.timestamp);
    var currentDate = new DateTime.now();
    if (syncDate.day == currentDate.day) {
      for (var v in Global.currentGames) {
        gameAvailable.raffles.putIfAbsent(v['name'], () => v['raffles']);
        raffles.add(v['raffles']);
        var chips = new List();
        chips = v['chips'];
        List<LotoChip> chipsGame = new List();
        for (var c in chips) {
          LotoChip chip = new LotoChip();
          chip.selected = false;
          chip.number = c['name'];
          chip.id = c['id'];
          chip.gameOptionId = v['gameMode']['id'];
          chip.code = c['code'];
          chip.codeName = v['code'];
          chip.gameId = v['id'];
          chip.game = v['name'];
          chip.description = c['description'];
          chip.image = 'assets/chips/' +
              c['description'].toString().toLowerCase().replaceAll('ñ', 'n') +
              '.png';
          setState(() {
            chipsGame.add(chip);
          });
        }
        gameAvailable.chips.putIfAbsent(v['name'], () => chipsGame);
      }
      populateRaffles(raffles);
    } else {
      Fluttertoast.showToast(
          msg: "Se deben actualizar las jugadas",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  void populateRaffles(List raffles) async {
    this.raffles = new List();
    List tempRaffles = [];
    for (var r in raffles) {
      tempRaffles.addAll(r);
    }
    Map tempRaffles2 = new Map();
    for (var r in tempRaffles) {
      tempRaffles2.putIfAbsent(r['code'], () => r);
    }
    Map newMap = Map.fromEntries(tempRaffles2.entries.toList()
      ..sort((e1, e2) => (e1.key).compareTo((e2.key))));
    newMap.forEach((key, value) {
      if (value['active'] == 1) {
        int raffleTime = (key);
        int currentTime = Time.getCurrentMinutesInDay();
        if (raffleTime > currentTime) {
          this.raffles.add(key);
        }
      }
    });
  }

  void updateAmount(List<PreviewPlay> p) {
    int amount = 0;
    for (PreviewPlay p in previewPlays) {
      amount += p.amount;
    }
    setState(() {
      sellAmount = amount;
    });
  }

  void deletePlay(PreviewPlay previewPlay) {
    setState(() {
      previewMapPlays.remove(previewMapPlays.keys.firstWhere(
          (element) => previewMapPlays[element] == previewPlay,
          orElse: () => null));
      addPlayToList();
    });
  }

  void editAmount(PreviewPlay previewPlay) {
    showDialog(
        context: context,
        builder: (_) {
          return EditDialog(
            previewPlay: previewPlay,
            onAmountEditedChanged: (PreviewPlay previewPlay) {
              setState(() {
                previewMapPlays[(previewMapPlays.keys.firstWhere(
                    (element) => previewMapPlays[element] == previewPlay,
                    orElse: () => null))] = previewPlay;
                addPlayToList();
              });
            },
          );
        });
  }

  void clearAll() {
    setState(() {
      isRandom = false;
      previewPlays.clear();
      previewMapPlays.clear();
    });
    updateAmount(previewPlays);
  }

  void addPlaysToPreview(List<PreviewPlay> p) {
    for (PreviewPlay play in p) {
      String gamePlay = play.chip.game +
          ' ' +
          play.raffle.toString() +
          ' ' +
          play.chip.number +
          ' ' +
          play.chip.description;
      if (previewMapPlays.containsKey(gamePlay)) {
        PreviewPlay previewPlay = new PreviewPlay();
        previewPlay = previewMapPlays[gamePlay];
        previewPlay.amount += play.amount;
        previewMapPlays[gamePlay] = previewPlay;
      } else {
        previewMapPlays[gamePlay] = play;
      }
    }
    addPlayToList();
  }

  void addPlayToList() {
    previewPlays.clear();
    previewPlays.addAll(previewMapPlays.values.toList());
    updateAmount(previewPlays);
  }

  void makePlay(List<PreviewPlay> previewPlays) async {
    if (previewPlays.length > 0) {
      for (PreviewPlay p in previewPlays) {
        Raffle.findIdByRaffle(p);
      }
      List<SellItem> sellItems = new List();
      TPHelper optionGameMap = new TPHelper();
      Map<int, TPHelper> raffleMap = new Map();
      Map<dynamic, List<SellItem>> r = new Map();
      Map optionGameId = new Map();
      for (PreviewPlay p in previewPlays) {
        var chip = {
          'active': 0,
          'code': p.chip.code,
          "description": p.chip.description,
          "id": p.chip.id,
          "number": p.chip.number
        };
        SellItem sellItem = new SellItem();
        sellItem.raffle = p.raffleId;
        sellItem.amount = p.amount;
        sellItem.effectiveSell = 0;
        sellItem.name = p.chip.game;
        sellItem.gameOptionId = p.chip.gameOptionId;
        sellItem.description = p.chip.description;
        sellItem.chip = chip;
        sellItem.codeName = p.chip.codeName;
        sellItems.add(sellItem);
        try {
          if (r.containsKey(sellItem.raffle['id'])) {
            List<SellItem> items = r[sellItem.raffle['id']];
            items.add(sellItem);
            r[sellItem.raffle['id']] = items;
            optionGameId[sellItem.gameOptionId] = r;
          } else {
            List<SellItem> items = new List();
            items.add(sellItem);
            r.putIfAbsent(sellItem.raffle['id'], () => items);
            optionGameId[sellItem.gameOptionId] = r;
          }
        } catch (e) {
          e.toString();
        }
      }
      optionGameId.forEach((key, value) {
        value.forEach((keyV, valueV) {
          optionGameMap = new TPHelper();
          optionGameMap.addTPHelper(key, valueV);
          raffleMap[keyV] = optionGameMap;
        });
      });
      Sell sell = new Sell();
      sell.cash = Global.cash;
      sell.promotional = Global.promotional;
      sell.totalAmount = getTotalAmount(sellItems);
      sell.userId = Global.userId;
      sell.s = sellItems;
      sell.sellItems = raffleMap;
      sell.xSerial = new DateTime.now().millisecondsSinceEpoch.toString();
      showDialog(
          context: context,
          builder: (_) {
            return ConfirmPlaysDialog(
              sell: sell,
              isConfirm: false,
              onConfirmPlay: (Sell sell) {
                confirmPlays(sell);
              },
            );
          });
    } else {
      Fluttertoast.showToast(
          msg: "Debe agregar una jugada",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  int getTotalAmount(List sellItems) {
    int amount = 0;
    for (SellItem sellItem in sellItems) {
      amount += sellItem.amount;
    }
    return amount;
  }

  void confirmPlays(Sell sell) async {
    try {
      showProgressDialog();
      var response = await HttpRequest.postRequestWithAuth(
          Api.urlSell, new Map(), json.encode(sell));
      handleResponse(response);
    } catch (e) {
      cancelProgressDialog();
      Fluttertoast.showToast(
          msg: "Algo salio mal",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  void handleResponse(final response) async {
    cancelProgressDialog();
    try {
      if (response != null && response != '') {
        Map<String, dynamic> map = json.decode(utf8.decode(response.bodyBytes));
        if (map['message'] != "" && map['error'] == 'OK') {
          Map message = (map['message']);
          Global.cash = message['cash'];
          Global.promotional = message['promotional'];
          Sell sell = new Sell();
          sell.xSerial = message['xSerial'];
          sell.cash = message['cash'];
          sell.totalAmount = message['totalAmount'];
          sell.promotional = message['promotional'];
          sell.userId = message['userId'];
          Map itemsMap = message['sellItems'];
          itemsMap.forEach((key, value) {
            value.forEach((keyV, valueV) {
              for (var s in valueV) {
                SellItem sellItem = new SellItem();
                var c = LotoChip.getChipById(s['chip']['id']);
                sellItem.raffle = Raffle.findRaffleById(key);
                sellItem.amount = s['amount'];
                sellItem.description = s['chip']['description'];
                sellItem.effectiveSell = s['effectiveSell'];
                sellItem.chip = s['chip'];
                sellItem.chip['number'] = c['name'];
                sellItem.name = c['game'];
                sell.s.add(sellItem);
              }
            });
          });
          clearAll();
          showDialog(
              context: context,
              builder: (_) {
                return ConfirmPlaysDialog(
                  sell: sell,
                  isConfirm: true,
                  onConfirmPlay: (Sell sell) {},
                );
              });
        } else if (map['message'] == ResponseConstants.authProblem) {
          MainScreenState.showLogoutDialog(context);
        }
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        key: scaffoldKey,
        body: ModalProgressHUD(
          child: Container(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Card(
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      elevation: 5,
                      margin: EdgeInsets.all(8),
                      child: Container(
                        margin: EdgeInsets.all(8),
                        width: screenSize.width / 2.1,
                        height: screenSize.height / 5,
                        child: new ListView.builder(
                          itemCount: raffles.length,
                          itemBuilder: (BuildContext context, int index) {
                            var raffle = raffles[index];
                            return GestureDetector(
                              child: Container(
                                margin: EdgeInsets.all(8),
                                child: Text(
                                  'Sorteo: ' +
                                      Conversions.getRaffleHour(((raffle))),
                                ),
                              ),
                              onTap: () {
                                getData();
                                var r = raffles[index];
                                isRandom
                                    ? showDialog(
                                        context: context,
                                        builder: (_) {
                                          return RandomChipDialog(
                                            gameAvailable: gameAvailable,
                                            raffle: r,
                                            names: GameAvailable
                                                .getAvailableGameNames(r,
                                                    gameAvailable),
                                            headerImages: GameAvailable
                                                .getAvailableGameImage(r,
                                                    gameAvailable),
                                            onRandomCountSelected:
                                                (List<PreviewPlay> p) {
                                              setState(() {
                                                isRandom = false;
                                                setState(() {
                                                  addPlaysToPreview(p);
                                                });
                                              });
                                            },
                                          );
                                        })
                                    : showDialog(
                                        context: context,
                                        builder: (_) {
                                          return ChipsDialog(
                                            raffle: r,
                                            chips:
                                                GameAvailable.getAvailableChips(
                                                    (r), gameAvailable),
                                            columnCount: gameAvailable.count,
                                            headerImages: GameAvailable
                                                .getAvailableGameImage(
                                                    (r), gameAvailable),
                                            onSelectedChipsChanged:
                                                (List<PreviewPlay> p) {
                                              setState(() {
                                                addPlaysToPreview(p);
                                              });
                                            },
                                          );
                                        });
                              },
                            );
                          },
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            elevation: 5,
                            margin: EdgeInsets.all(10),
                            child: Container(
                              width: screenSize.width / 2.8,
                              height: screenSize.height / 10,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Monto disponible',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Text(
                                    "${moneyFormatter.format(Global.cash)}",
                                    style: TextStyle(
                                        color: Colors.green, fontSize: 22),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Card(
                            semanticContainer: true,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            elevation: 5,
                            margin: EdgeInsets.all(10),
                            child: Container(
                              width: screenSize.width / 2.8,
                              height: screenSize.height / 10,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Monto venta',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Text(
                                    "${moneyFormatter.format(sellAmount)}",
                                    style: TextStyle(
                                        color: Colors.deepOrangeAccent,
                                        fontSize: 22),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 5,
                  margin: EdgeInsets.all(8),
                  child: Container(
                    margin: EdgeInsets.all(8),
                    height: screenSize.height / 2.4,
                    child: new ListView.builder(
                      itemCount: previewPlays.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          child: Container(
                              margin: EdgeInsets.all(8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(previewPlays[index]
                                          .chip
                                          .game
                                          .substring(0, 1)
                                          .toUpperCase() +
                                      previewPlays[index]
                                          .chip
                                          .game
                                          .substring(1) +
                                      ' ' +
                                      Conversions.getRaffleHour(
                                          previewPlays[index].raffle) +
                                      '\n[ ' +
                                      previewPlays[index].chip.number +
                                      ' ] ' +
                                      previewPlays[index].chip.description +
                                      ' x ' +
                                      previewPlays[index].amount.toString()),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                          width: 24,
                                          height: 24,
                                          child: Image.asset(removeDiacritics(
                                              previewPlays[index].chip.image))),
                                      IconButton(
                                        icon: Icon(Icons.delete),
                                        onPressed: () {
                                          deletePlay(previewPlays[index]);
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          onTap: () {
                            editAmount(previewPlays[index]);
                          },
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          inAsyncCall: isInAsyncCall,
          opacity: 0.5,
          progressIndicator: NativeLoadingIndicator(),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.cancel, color: Colors.white),
                  onPressed: () {
                    checkData();
                    //clearAll();
                  },
                ),
                IconButton(
                  icon: Icon(Icons.casino,
                      color: isRandom ? Colors.white : Colors.grey),
                  onPressed: () {
                    setState(() {
                      isRandom = !isRandom;
                    });
                  },
                ),
                IconButton(
                  icon: Icon(Icons.play_arrow, color: Colors.white),
                  onPressed: () {
                    makePlay(previewPlays);
                  },
                ),
              ],
            ),
          ),
        ),
        resizeToAvoidBottomPadding: false);
  }
}