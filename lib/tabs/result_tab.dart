import 'dart:convert';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:lotosuiteplay/config/assest.dart';
import 'package:lotosuiteplay/request/api_constants.dart';
import 'package:lotosuiteplay/request/http_request.dart';
import 'package:lotosuiteplay/utils/conversions.dart';
import 'package:lotosuiteplay/utils/time.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResultTab extends StatefulWidget {
  @override
  ResultTabState createState() => ResultTabState();
}

class ResultTabState extends State<ResultTab> with AutomaticKeepAliveClientMixin<ResultTab>{
  bool isInAsyncCall = false;
  String dateToConsult;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List results = new List();
  var sharedPreferences;
  bool isHistory = false;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    requestLastResults();
    //initVars();
  }

  void initVars() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.get('results') != null) {
      var messageDecode = json.decode(sharedPreferences.get('results'));
      if (messageDecode.length > 0) {
        messageDecode.forEach((key, value) {
          results.add(value);
          print('paso aqui');
          print(results);
        });
        cancelProgressDialog();
      }
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  requestLastResults() {
    dateToConsult = Time.getCurrentDateInString();
    requestResults();
  }

  requestResults() async {
    results.clear();
    Map map = {'startDate': dateToConsult, 'endDate': dateToConsult};
    showProgressDialog();
    var response = await HttpRequest.postRequestWithAuth(
        Api.urlGetResult, new Map(), json.encode(map));
    handleResultResponse(response);
  }

  handleResultResponse(final response) async {
    cancelProgressDialog();
    sharedPreferences = await SharedPreferences.getInstance();
    if (response != '' && response != null) {
      Map<String, dynamic> map = json.decode(response.body);
      if (map['message'] != "" && map['error'] == '') {
        Map messageDecode = json.decode(map['message']);
        var sortMap = Map.fromEntries(messageDecode.entries.toList()
          ..sort((e1, e2) => int.parse(e2.key).compareTo(int.parse(e1.key))));
        if (!isHistory) {
          //messageDecode.putIfAbsent('date', () => dateToConsult);
          sharedPreferences.setString('results', json.encode(sortMap));
        }
        if (sortMap.length > 0) {
          sortMap.forEach((key, value) {
            results.add(value);
            print(results);
          });
        } else {
          if (sharedPreferences.get('results') != null) {}
        }
      } else if (map['error'] != '') {
        if (map['error'] == 'no-content') {
          showInSnackBar('No se encontraron resultados');
        }
      }
    }
  }

  selectDate() {
    DateTime dateTime = new DateTime.now();
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(dateTime.year, dateTime.month - 6),
        maxTime: DateTime(dateTime.year, dateTime.month, dateTime.day),
        onChanged: (date) {}, onConfirm: (date) {
      final f = new DateFormat('dd-MM-yy');
      dateToConsult = f.format(date);
      requestResults();
      print('confirm $date');
    }, currentTime: DateTime.now(), locale: LocaleType.es);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    width: 60,
                    height: 36,
                    decoration: new BoxDecoration(
                        image: DecorationImage(
                      image: AssetImage('assets/safari.png'),
                    )),
                  ),
                  Container(
                    width: 60,
                    height: 36,
                    decoration: new BoxDecoration(
                        image: DecorationImage(
                      image: AssetImage('assets/tizana.png'),
                    )),
                  ),
                  Container(
                    width: 60,
                    height: 36,
                    decoration: new BoxDecoration(
                        image: DecorationImage(
                      image: AssetImage('assets/loto.png'),
                    )),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 1.7,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.all(8),
              child: ListView.builder(
                  itemCount: results.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Text('Sorteo: ' +
                                    int.parse(results.toList()[index]
                                            ['resultMobiles'][0]['serial'])
                                        .toString()),
                                Text(dateToConsult),
                                Text(Conversions.getRaffleHour(
                                    int.parse(results.toList()[index]['hour'])))
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: getChips(
                                  results.toList()[index]['resultMobiles']),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.refresh, color: Colors.white),
                onPressed: () {
                  requestLastResults();
                },
              ),
              IconButton(
                icon: Icon(Icons.cancel, color: Colors.white),
                onPressed: () {
                  //discardTicket();
                },
              ),
              IconButton(
                icon: Icon(Icons.search, color: Colors.white),
                onPressed: () {
                  selectDate();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> getChips(var results) {
    List<Widget> childs = new List();
    var uniqueIds = results.map((o) => o['label']).toSet();
    /*for (var c in results) {
      childs.add(Card(
        child: Column(
          children: <Widget>[
            Container(
              width: 80,
              height: 50,
              decoration: new BoxDecoration(
                  image: DecorationImage(
                image: AssetImage(Assets.getAssetName(
                    removeDiacritics(c['label'])
                        .toString()
                        .split('-')[1]
                        .toLowerCase()
                        .trim())),
              )),
            ),
            Text(c['label']),
          ],
        ),
      ));
    }*/
    for (var c in uniqueIds) {
      print('algo');
      print(c);
      childs.add(Card(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8),
              width: 80,
              height: 50,
              decoration: new BoxDecoration(
                  image: DecorationImage(
                image: AssetImage(Assets.getAssetName(removeDiacritics(c)
                    .toString()
                    .split('-')[1]
                    .toLowerCase()
                    .trim())),
              )),
            ),
            Text(c),
          ],
        ),
      ));
    }
    return childs;
  }
}
