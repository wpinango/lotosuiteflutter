import 'dart:convert';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:lotosuiteplay/config/assest.dart';
import 'package:lotosuiteplay/dialogs/dialog.dart';
import 'package:lotosuiteplay/models/chip.dart';
import 'package:lotosuiteplay/request/api_constants.dart';
import 'package:lotosuiteplay/request/http_request.dart';
import 'package:lotosuiteplay/request/response_constants.dart';
import 'package:lotosuiteplay/screens/main_screen/main_screen.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';

class TicketsTab extends StatefulWidget {
  @override
  TicketsTabState createState() => TicketsTabState();
}

class TicketsTabState extends State<TicketsTab>
    with AutomaticKeepAliveClientMixin<TicketsTab> {
  bool isInAsyncCall = false;
  String dateToConsult;
  int starDate, endDate;
  List tickets = new List();
  List items = new List();
  var details = new Map();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final moneyFormatter = new NumberFormat("#,###", "de_DE");

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    requestLastTickets();
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  requestLastTickets() {
    DateTime now = new DateTime.now();
    starDate = new DateTime(now.year, now.month, now.day, 0, 0, 0)
        .millisecondsSinceEpoch;
    endDate = new DateTime(now.year, now.month, now.day, now.hour, now.minute + 1, now.second)
        .millisecondsSinceEpoch;
    requestTickets();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  requestTickets() async {
    Map map = {'startDate': starDate, 'endDate': endDate};
    showProgressDialog();
    var response = await HttpRequest.postRequestWithAuth(
        Api.urlAllTicket, new Map(), json.encode(map));
    handleTicketResponse(response);
  }

  handleTicketResponse(final response) async {
    cancelProgressDialog();
    tickets.clear();
    items.clear();
    if (response != '' && response != null) {
      Map<String, dynamic> map = json.decode(utf8.decode(response.bodyBytes));
      if (map['message'] != "" && map['error'] == 'OK') {
        var ticketsDecode = map['message'];
        for (var t in ticketsDecode) {
          tickets.add(t);
        }
        for (var value in tickets) {
          value['selected'] = false;
          int count = 0;
          for (var ticket in value['ticketDetails']) {
            if (ticket['processed'] == 1) {
              count++;
            }
          }
          if (count == value['ticketDetails'].length) {
            value['status'] = 4;
          }
        }
        if (tickets.length > 0) {
          selectFirstTicket();
        }
      } else if (map['error'] != '') {
        if (map['error'] == 'no-content') {
          showInSnackBar('No se encontro tickets para esta fecha');
          setState(() {

          });
        } else if (map['error'] == ResponseConstants.authProblem) {
          MainScreenState.showLogoutDialog(context);
        }
      }
    }
  }

  selectFirstTicket() {
    getSelectedTicket(tickets[0]);
    tickets[0]['selected'] = true;
  }

  getSelectedTicket(var details) {
    setState(() {
      this.details = details;
      items.clear();
      if (details['ticketDetails'].length > 0) {
        items.addAll(details['ticketDetails']);
      }
    });
  }

  deselectAllTickets() {
    for (var item in tickets) {
      item['selected'] = false;
    }
  }

  String getSerialTicketSelected() {
    String serial = '';
    for (var item in tickets) {
      item['selected'] = false;
      if (item['selected']) {
        serial = item['id'];
      }
    }
    return serial;
  }

  discardTicket() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context, 'Alerta', 'Desea cancelar el ticket', 'Cerrar', 'Aceptar')) {
      showProgressDialog();
      String serial = getSerialTicketSelected();
      if (serial == '') {
        Fluttertoast.showToast(
            msg: "Debe seleccionar un ticket",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
        return;
      } else {
        Map body = {'xSerial': serial};
        var response = await HttpRequest.postRequestWithAuth(
            Api.urlDiscardTicket, new Map(), json.encode(body));
        handleDiscardTicketResponse(response);
      }
    }
  }

  handleDiscardTicketResponse(final response) {
    cancelProgressDialog();
    if (response != '' && response != null) {
      Map<String, dynamic> map = json.decode(response.body);
      if (map['message'] != "" && map['error'] == '') {
        Map responseDecode = json.decode(map['message']);
        if (responseDecode['status']) {
          Fluttertoast.showToast(
              msg: "Se elimino ticket",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
          String serial = getSerialTicketSelected();
          setState(() {
            /*tickets[serial]['status'] = 2;
            for (var item in tickets[serial]['items']) {
              item['status'] = 2;
            }*/
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: Container(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Card(
                    semanticContainer: true,
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 5,
                    margin: EdgeInsets.all(8),
                    child: Container(
                      margin: EdgeInsets.all(8),
                      width: screenSize.width / 2.1,
                      height: screenSize.height / 5,
                      child: new ListView.builder(
                        itemCount: tickets.length,
                        itemBuilder: (BuildContext context, int index) {
                          var ticket = tickets[index];
                          return GestureDetector(
                            child: Container(
                              color: ticket['selected'] == true
                                  ? Colors.lightBlueAccent
                                  : Colors.white,
                              margin: EdgeInsets.all(8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('Ticket: ' +
                                      ticket['id'].toString().padLeft(6, '0') +
                                      '\n'), // +
                                  //value['ticketDate'] +
                                  //' ' +
                                  // value['ticketTime']),
                                  Container(
                                      width: 24,
                                      height: 24,
                                      child: getTicketIcon(ticket))
                                ],
                              ),
                            ),
                            onTap: () {
                              deselectAllTickets();
                              ticket['selected'] = !ticket['selected'];
                              getSelectedTicket(tickets[index]);
                            },
                          );
                        },
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                          child: Container(
                            width: screenSize.width / 2.8,
                            height: screenSize.height / 10,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Monto Jugado',
                                  style: TextStyle(color: Colors.grey),
                                ),
                                Text(
                                    details['amount'] != null
                                        ? "${moneyFormatter.format(details['amount'])}"
                                        : '0',
                                    style: TextStyle(
                                        color: Colors.deepOrangeAccent,
                                        fontSize: 22))
                              ],
                            ),
                          ),
                        ),
                        Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                          child: Container(
                            width: screenSize.width / 2.8,
                            height: screenSize.height / 10,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Monto Premiado',
                                  style: TextStyle(color: Colors.grey),
                                ),
                                Text(
                                    details['award'] != null
                                        ? "${moneyFormatter.format(details['award'])}"
                                        : '0',
                                    style: TextStyle(
                                        color: Colors.green, fontSize: 22)
                                    )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(8),
                child: Container(
                  margin: EdgeInsets.all(8),
                  height: screenSize.height / 2.4,
                  child: new ListView.builder(
                    itemCount: items.length,
                    itemBuilder: (BuildContext context, int index) {
                      String image = Assets.getAssetName(
                          items[index]['chip']['description']
                              .trim()
                              .toLowerCase()
                              .replaceAll('ñ', 'n'));
                      String title =
                          LotoChip.getGameByChip(items[index]['chip']['id']) +
                              items[index]['raffle']['time'] +
                              '\n[' +
                              items[index]['chip']['name'] +
                              '] ' +
                              items[index]['chip']['description'] + ' x ' +
                              "${moneyFormatter.format(items[index]['amount'])}";
                      return GestureDetector(
                        child: Container(
                            color: getDetailColor(items[index]),
                            margin: EdgeInsets.all(4),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(title),
                                Row(
                                  children: <Widget>[
                                    Container(
                                        width: 24,
                                        height: 24,
                                        child: Image.asset(
                                            removeDiacritics(image))),
                                  ],
                                ),
                              ].where((child) => child != null).toList(),
                            )),
                      );
                    },
                  ),
                ),
              ),
            ].where((child) => child != null).toList(),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.refresh, color: Colors.white),
                onPressed: () {
                  requestLastTickets();
                },
              ),
              IconButton(
                icon: Icon(Icons.cancel, color: Colors.white),
                onPressed: () {
                  discardTicket();
                },
              ),
              IconButton(
                icon: Icon(Icons.search, color: Colors.white),
                onPressed: () {
                  selectDate();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  selectDate() {
    DateTime dateTime = new DateTime.now();
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(dateTime.year, dateTime.month - 6),
        maxTime: DateTime(dateTime.year, dateTime.month, dateTime.day),
        onChanged: (date) {}, onConfirm: (date) {
      final f = new DateFormat('dd-MM-yy');
      starDate = new DateTime(date.year, date.month, date.day, 0, 0, 0)
          .millisecondsSinceEpoch;
      endDate = new DateTime(date.year, date.month, date.day, 23, 59, 59)
          .millisecondsSinceEpoch;
      //dateToConsult = date.millisecondsSinceEpoch.toString(); //f.format(date);
      requestTickets();
    }, currentTime: DateTime.now(), locale: LocaleType.es);
  }

  Icon getTicketIcon(var detail) {
    Icon icon;
    if (detail['status'] == 0) {
      icon = new Icon(Icons.receipt);
    } else if (detail['status'] == 1) {
      icon = new Icon(
        Icons.card_giftcard,
        color: Colors.amber,
      );
    } else if (detail['status'] == 2) {
      icon = new Icon(
        Icons.close,
        color: Colors.red,
      );
    } else if (detail['status'] == 3) {
      icon = new Icon(
        Icons.done,
        color: Colors.green,
      );
    } else if (detail['status'] == 4) {
      icon = new Icon(
        Icons.receipt,
        color: Colors.grey,
      );
    }
    return icon;
  }

  Color getDetailColor(var item) {
    Color color = Colors.white;
    /*if (item['status'] == 0 && item['processed'] == 0) {
      color = Colors.white;
    } else if (item['status'] == 1 && item['processed'] == 1) {
      color = Colors.amber;
    } else if (item['status'] == 0 && item['processed'] == 1) {
      color = Colors.redAccent;
    } else if (item['status'] == 2) {
      color = Colors.grey;
    }*/
    return color;
  }
}
