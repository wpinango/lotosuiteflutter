import 'dart:ui';

import 'package:flutter/material.dart';

Color primaryColor =  Color(0xFf21272b);
Color secondaryColor =  Color(0xFf37474f);

TextStyle labelStyle = new TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.normal,
);