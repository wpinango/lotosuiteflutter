import 'package:intl/intl.dart';

class Conversions {


  static String getRaffleHour(int raffle) {
    double h = raffle / 60;
    int m = raffle % 60;

    String hl = h.truncate().toString();
    if (hl.length == 1) {
      hl = '0' + hl;
    }

    String ml = m.toString();
    if (ml.length == 1) {
      ml = '0' + ml;
    }
    DateTime d = new DateTime.now();
    DateTime dateTime = new DateTime(d.year, d.month, d.day, int.parse(hl), int.parse(ml));
    return new DateFormat('hh:mm a').format(dateTime);
  }
}