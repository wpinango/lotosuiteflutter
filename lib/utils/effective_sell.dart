import 'package:lotosuiteplay/models/sell.dart';
import 'package:lotosuiteplay/models/sell_item.dart';

class EffectiveSell{
  static Map checkEffectiveSell(Sell sell) {
    Map<String, int> values = new Map();
    values.putIfAbsent('sell', () => 0);
    values.putIfAbsent('no_sell', () => 0);
    for (SellItem s in sell.s) {
      if (s.amount == s.effectiveSell) {
        values['sell'] = values['sell'] + 1;
      } else {
        values['no_sell'] = values['no_sell'] + 1;
      }
    }
    return values;
  }
}