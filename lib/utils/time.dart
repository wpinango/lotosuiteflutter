import 'package:intl/intl.dart';

class Time {
  static int getCurrentMinutesInDay() {
    DateTime dateTime = new DateTime.now();
    int hour = dateTime.minute;
    int minutes = dateTime.hour * 60;
    int currentTime = hour + minutes;
    return currentTime;
  }

  static String getCurrentDateInString() {
    DateTime dateTime = new DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy');
    return formatter.format(dateTime);
  }
}