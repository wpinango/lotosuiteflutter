import 'package:lotosuiteplay/models/sell_item.dart';
import 'package:lotosuiteplay/models/tp_helper.dart';
import 'package:lotosuiteplay/models/tp_helper_item.dart';
import 'package:quiver/collection.dart';

class TriplePlayAvailable {

  static int isTicketAvailableTPWinner(List<SellItem> sellItems) {
    // 0-tp, 1-no Tp, 2-no Tp
    int valueReturn = 4;
    Map<int, Map<String, int>> test = new Map();
    for (int i = 0; i < sellItems.length; i++) {
      Map<String, int> t = test[sellItems[i].raffle['code']];
      if (t == null) {
        t = new Map();
        t[sellItems[i].codeName] =  1;
      } else {
        if (t[sellItems[i].codeName] == null) {
          t[sellItems[i].codeName] =  1;
        } else {
          t[sellItems[i].codeName] =  t[sellItems[i].codeName] + 1;
        }
      }
      test[sellItems[i].raffle['code']]  = t;
    }
    /*TPHelperItem tpHelper = new TPHelperItem();
    test.forEach((key, value) {
      value.forEach((keyValue, valueValue) {
        tpHelper.gameCode = keyValue;
        tpHelper.chipsSelected = valueValue;
        tpHelpers.addTPHelper(key, tpHelper);
      });
    });*/
    for (var t in test.values) {
      var values = t;
      for (var valueValue in values.values) {
        if (valueValue > 9) {
          valueReturn = 1;
          break;
        } else if (valueValue <= 9 && values.length == 3) {
          valueReturn = 0;
        }
      }
    }
    return valueReturn;
  }
}